* Consider trying to make all unix-domain connections to syslog per a program
use a single unified syslog instance that simply provides the correct name to
the header.

* I'd consider windows support but the last time I looked into their event logger
on windows 7 I basically decided to stop supporting windows.

* Test on alternative syslog daemons and ensure functionality is shared.

* Try to figure out a way to determine how close to the RFC to get since it
seems rsyslog is just barely using it and that is confusing as hell.

* Example code for documentation beyond the unittest?

