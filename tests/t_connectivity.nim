# This file is a part of nimsyslog.
#
# nimsyslog is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as
# published by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# nimsyslog is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with nimsyslog.  If not, see <http://www.gnu.org/licenses/>.

import nimsyslog
import unittest

const
  UDP_PORT = 514
  TCP_PORT = 514

suite "Testing local unix socket connection to syslog":
  setup:
    echo "Initializing connection to syslog"
    var syslog = newSyslogHandle("syslog_unittest")
    assert(nil != syslog)
  
  teardown:
    echo "Releasing connection from syslog"
    syslog.freeSyslogHandle()
  
  test "Sending debug message to User1":
    syslog.print("local unix socket connection test", Debug)


suite "Testing local UDP connection to syslog":
  setup:
    echo "Initializing connection to syslog"
    var syslog = newSyslogHandle("syslog_unittest", "localhost", Port(UDP_PORT), slkUDP)
    # Don't test for connectivity because UDP is connectionless
  
  teardown:
    echo "Releasing connection from syslog"
    syslog.freeSyslogHandle()
  
  test "Sending debug message to User1":
    syslog.print("local UDP connection test", Debug)


suite "Testing local TCP connection to syslog":
  setup:
    echo "Initializing connection to syslog"
    var syslog: SyslogHandle
    try:
      syslog = newSyslogHandle("syslog_unittest", "localhost", Port(TCP_PORT), slkTCP)
    except OSError as err:
      if err.errorCode == 111:
        echo "Unable to connect but seems to be working. Try adjusting your ports in the const section above"
        quit(0)
      else:
        echo "Got error code: ", err.errorCode
        raise
    assert(nil != syslog)
  
  teardown:
    echo "Releasing connection from syslog"
    syslog.freeSyslogHandle()
  
  test "Sending debug message to User1":
    syslog.print("local TCP connection test", Debug)


    