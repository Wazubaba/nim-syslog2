## Nim Syslog Interface
This is an early attempt at a syslog interface implemented entirely in
nim. Currently it has only been tested with [rsyslog][1] to work.

It is capable of connecting to a syslog daemon via Unix-Domain sockets,
UDP sockets, and TCP sockets.

Currently this library is considered a tentative 0.9 as it is pretty much
feature-complete as far as I am aware and should only need light updates
as Nim's syntax changes and grows.

All functions and pertinent types have been documented via nim doc comments,
and a unittest exists which can also help explain how to use this library.

[1]:https://github.com/rsyslog/rsyslog


## Important
Please note that if the unittest fails, it could simply be that either
your syslog daemon is not configured to use udp/tcp or that the ports
are different. At the top of the unittest is a consts section for
adjusting them to work with your system.


## Build info
I'm using nake to generate the docs, but this is optional. You can just
use `nimble doc2 src/nimsyslog.nim` if you'd prefer, I just find that
nimble's nimscript support is terrible and nake is a universally superior
build system anyways. Also I don't like dumping generated files into my
source directory.

To get nake, you can use nimble: `nimble install nake`

To only build docs with nake you can just do `nake doc`, or you can
build docs and run the unittests with the default rule invoked by `nake`.

I also have a clean rule with nake to purge all generated build files
and data for you: `nake clean`. This won't remove cached data in your
nimcache though because I have no way of knowing if `t_connectivity_[rd]`
is my unittest or something of yours.

