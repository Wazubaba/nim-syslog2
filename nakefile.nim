# This file is a part of nimsyslog.
#
# nimsyslog is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as
# published by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# nimsyslog is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with nimsyslog.  If not, see <http://www.gnu.org/licenses/>.

import nake
# I'm using nake for this because nimble's nimscript thing is janky as
# fuck. Nake is better anyways...

const outPath = "docs"/"generated"/"nimsyslog.html"

task "default", "Build documentation and run unittests":
  runTask("doc")
  runTask("test")

task "doc", "Build documentation":
  direShell(nimExe, "doc2", "--out:" & outPath, "src"/"nimsyslog.nim")

task "clean", "Clean generate files":
  withDir("tests"):
    removeFile("t_connectivity")
  withDir("docs"):
    removeDir("generated")
  removeFile("nakefile")

task "test", "Run unittests":
  direShell("nimble", "test")

